import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ServiceService {

  dadosDeputao: any;
  despesesDeputado: any;


  constructor(private http: HttpClient) { }

  listaDaputados() {
    require('./package.json');
    return this.http.get("https://dadosabertos.camara.leg.br/api/v2/deputados?ordem=ASC&ordenarPor=nome");
  }

  listaDaputadosPorNome(nome) {
    return this.http.get("https://dadosabertos.camara.leg.br/api/v2/deputados?nome="+nome+"&ordem=ASC&ordenarPor=nome");
  }

  infromacaoDeputado(id) {
    return this.http.get("https://dadosabertos.camara.leg.br/api/v2/deputados/" + id);
  }

  dadosPartido(id) {
    return this.http.get("https://dadosabertos.camara.leg.br/api/v2/deputados/" + id);
  }

  despesasDaputado(id) {
    return this.http.get("https://dadosabertos.camara.leg.br/api/v2/deputados/" + id + "/despesas?ordem=DESC&ordenarPor=ano");

  }

  pesquisardeputadoPorNome(nome) {
    return this.http.get("https://dadosabertos.camara.leg.br/api/v2/deputados?nome=" + nome + "&ordem=ASC&ordenarPor=nome");

  }

}
