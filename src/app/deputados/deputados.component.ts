import { ServiceService } from './../service.service';
import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  name: string;
  siglaPartido: string;
  email: string;
  uf: string;
}

@Component({
  selector: 'app-deputados',
  templateUrl: './deputados.component.html',
  styleUrls: ['./deputados.component.css'],
  styles: [`
      :host ::ng-deep .ui-button {
          margin: .5em .5em .5em 0;
          width: 140px;
      }
      @media screen and (max-width: 40em) {
          :host ::ng-deep .ui-dialog {
              width: 75vw !important;
          }
      }
  `]
})

export class DeputadosComponent implements OnInit {

  constructor(private service: ServiceService) { }

  listaDeputadoData: any;
  listaDeputado: any;
  displayMaximizable: boolean;
  paginaAtual = 1;
  displayedColumns: string[] = ['nome', 'siglaPartido', 'email', 'uf'];
  nomeParlamentar: any;

  ngOnInit(): void {
    this.listaDaputados();
  }

  dadosdoDeputado(item) {
    this.displayMaximizable = true;
    this.service.dadosDeputao = item;
  }

  dadosdopartido(item) {
    this.service.dadosPartido(item.id)
  }

  pesquisardeputadoPorNome(nome) {
    if (nome === null || nome === '') {
      this.listaDaputados();
    } else {
      this.service.listaDaputadosPorNome(nome)
        .subscribe(data => {
          this.listaDeputadoData = data;
          this.listaDeputado = this.listaDeputadoData.dados;
          console.log(this.listaDeputado)
        })
    }

  }

  listaDaputados() {
    this.service.listaDaputados()
      .subscribe(data => {
        this.listaDeputadoData = data;
        this.listaDeputado = this.listaDeputadoData.dados;
      })
  }

}
