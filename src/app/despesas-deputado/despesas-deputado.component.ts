import { ServiceService } from './../service.service';
import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-despesas-deputado',
  templateUrl: './despesas-deputado.component.html',
  styleUrls: ['./despesas-deputado.component.css']
})
export class DespesasDeputadoComponent implements OnInit {

  nomeParlamentar: any;
  listaParlamentar: any;
  displayMaximizable: boolean;


  constructor(private service: ServiceService, private router: Router) { }

  ngOnInit(): void {
  }

  pesquisardeputadoPorNome(nome) {
    this.service.pesquisardeputadoPorNome(nome)
      .subscribe(data => {
        this.listaParlamentar = data;
        this.listaParlamentar = this.listaParlamentar.dados;
      })
  }

  dadosdoDespesaDeputado(item) {
    this.displayMaximizable = true;
    this.service.despesesDeputado = item;

    this.router.navigate(['/despesasGrafico']);
  }

  despesasDaputado(item) {
    this.service.dadosDeputao = item;
    this.service.despesasDaputado(item.id)
      .subscribe(data => {
        this.dadosdoDespesaDeputado(data);
      })
  }

}
