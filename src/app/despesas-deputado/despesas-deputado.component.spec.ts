import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DespesasDeputadoComponent } from './despesas-deputado.component';

describe('DespesasDeputadoComponent', () => {
  let component: DespesasDeputadoComponent;
  let fixture: ComponentFixture<DespesasDeputadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DespesasDeputadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DespesasDeputadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
