import { ServiceService } from './../service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dados-deputado',
  templateUrl: './dados-deputado.component.html',
  styleUrls: ['./dados-deputado.component.css']
})
export class DadosDeputadoComponent implements OnInit {

  dadosDeputado: any;
  infoemacaodeputado: any;

  constructor(private service: ServiceService) { }

  ngOnInit(): void {
    this.dadosDeputado = this.service.dadosDeputao
    this.informacaoDeputao(this.dadosDeputado.id);
  }

  informacaoDeputao(id) {
    this.service.infromacaoDeputado(id)
      .subscribe(data => {
        console.log(data)
        this.infoemacaodeputado = data
        this.infoemacaodeputado = this.infoemacaodeputado.dados;
      })
  }

}
