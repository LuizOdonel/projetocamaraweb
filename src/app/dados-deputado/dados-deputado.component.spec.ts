import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DadosDeputadoComponent } from './dados-deputado.component';

describe('DadosDeputadoComponent', () => {
  let component: DadosDeputadoComponent;
  let fixture: ComponentFixture<DadosDeputadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DadosDeputadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosDeputadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
