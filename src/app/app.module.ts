import { MatDialogModule } from '@angular/material/dialog';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatTableModule} from '@angular/material/table';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { DeputadosComponent } from './deputados/deputados.component';
import { HttpClientModule } from '@angular/common/http';
import { DialogModule } from 'primeng/dialog';
import {MatIconModule} from '@angular/material/icon';
import {MatTabsModule} from '@angular/material/tabs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DadosDeputadoComponent } from './dados-deputado/dados-deputado.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DespesasDeputadoComponent } from './despesas-deputado/despesas-deputado.component';
import { routing } from './app.routing';
import { FormsModule } from '@angular/forms';
import { GraficoDespesasParlamentarComponent } from './grafico-despesas-parlamentar/grafico-despesas-parlamentar.component';
import { NgxPaginationModule } from 'ngx-pagination';
import {ChartModule} from 'primeng/chart';
import {ToastModule} from 'primeng/toast';
import {DropdownModule} from "primeng/dropdown";

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    DeputadosComponent,
    DadosDeputadoComponent,
    DespesasDeputadoComponent,
    GraficoDespesasParlamentarComponent
  ],
  imports: [
    DropdownModule,
    ToastModule,
    ChartModule,
    NgxPaginationModule,
    routing,
    MatTabsModule,
    MatIconModule,
    BrowserModule,
    HttpClientModule,
    MatTableModule,
    MatDialogModule,
    DialogModule,
    BrowserAnimationsModule,
    NgbModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
