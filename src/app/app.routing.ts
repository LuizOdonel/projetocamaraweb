import { GraficoDespesasParlamentarComponent } from './grafico-despesas-parlamentar/grafico-despesas-parlamentar.component';
import { MenuComponent } from './menu/menu.component';
import { DespesasDeputadoComponent } from './despesas-deputado/despesas-deputado.component';
import { Routes, RouterModule } from '@angular/router';
import { DeputadosComponent } from './deputados/deputados.component';
import { ModuleWithProviders } from '@angular/compiler/src/core';

const APP_ROUTES: Routes = [
    {path: '', component: DeputadosComponent},
    {path: 'despesas', component: DespesasDeputadoComponent},
    {path: 'despesasGrafico', component: GraficoDespesasParlamentarComponent}

];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);