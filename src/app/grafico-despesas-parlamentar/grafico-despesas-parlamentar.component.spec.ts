import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraficoDespesasParlamentarComponent } from './grafico-despesas-parlamentar.component';

describe('GraficoDespesasParlamentarComponent', () => {
  let component: GraficoDespesasParlamentarComponent;
  let fixture: ComponentFixture<GraficoDespesasParlamentarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraficoDespesasParlamentarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraficoDespesasParlamentarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
