import { ServiceService } from './../service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grafico-despesas-parlamentar',
  templateUrl: './grafico-despesas-parlamentar.component.html',
  styleUrls: ['./grafico-despesas-parlamentar.component.css']
})
export class GraficoDespesasParlamentarComponent implements OnInit {

  graficoPizza: any;
  data: any;
  dadosparlamentar: any;
  despesasParlamentar: any;
  paginaAtual = 1;
  valorDespesaMensal = 0;
  nomeDespesa = '';
  nomeDespesa1 = '';
  nomeDespesa2 = '';
  nomeDespesa3 = '';
  nomeDespesa4 = '';
  totalDespesa = 0;
  totalDespesa1 = 0;
  totalDespesa2 = 0;
  totalDespesa3 = 0;
  totalDespesa4 = 0;


  constructor(private servico: ServiceService) {

  }

  somaValorMensalDespesas(mes, ano) {
    this.valorDespesaMensal = 0;
    for (let index = 0; index < this.despesasParlamentar.length; index++) {
      if (mes === this.despesasParlamentar[index].mes && ano === this.despesasParlamentar[index].ano) {
        this.valorDespesaMensal = this.despesasParlamentar[index].valorDocumento + this.valorDespesaMensal;
      }

    }
    return this.valorDespesaMensal;
  }

  tiposDespesas() {

    for (let index = 0; index < this.despesasParlamentar.length; index++) {

      switch (this.despesasParlamentar[index].codTipoDocumento) {
        case 0:
          this.nomeDespesa = this.despesasParlamentar[index].tipoDocumento;
          if (this.nomeDespesa === undefined) {
            this.nomeDespesa = '';
          }
          this.totalDespesa = this.totalDespesa + 1;
          break;

        case 1:
          this.nomeDespesa1 = this.despesasParlamentar[index].tipoDocumento;
          if (this.nomeDespesa1 === undefined) {
            this.nomeDespesa1 = '';
          }
          this.totalDespesa1 = this.totalDespesa1 + 1;
          break;

        case 4:
          this.nomeDespesa4 = this.despesasParlamentar[index].tipoDocumento;
          if (this.nomeDespesa4 === undefined) {
            this.nomeDespesa4 = '';
          }
          this.totalDespesa4 = this.totalDespesa4 + 1;
          break;

        default:
          return null;
      }


    }
    return this.valorDespesaMensal;
  }

  ngOnInit(): void {
    this.dadosparlamentar = this.servico.dadosDeputao;
    this.despesasParlamentar = this.servico.despesesDeputado;
    this.despesasParlamentar = this.despesasParlamentar.dados;

    this.tiposDespesas();

    this.data = {
      labels: [this.despesasParlamentar[0].mes + '/' + this.despesasParlamentar[0].ano,
      this.despesasParlamentar[1].mes + '/' + this.despesasParlamentar[1].ano,
      this.despesasParlamentar[2].mes + '/' + this.despesasParlamentar[2].ano,
      this.despesasParlamentar[3].mes + '/' + this.despesasParlamentar[3].ano,
      this.despesasParlamentar[4].mes + '/' + this.despesasParlamentar[4].ano,
      this.despesasParlamentar[5].mes + '/' + this.despesasParlamentar[5].ano],
      datasets: [
        {
          label: 'Gasto total',
          backgroundColor: '#42A5F5',
          borderColor: '#1E88E5',
          data: [
            this.somaValorMensalDespesas(this.despesasParlamentar[0].mes, this.despesasParlamentar[0].ano),
            this.somaValorMensalDespesas(this.despesasParlamentar[1].mes, this.despesasParlamentar[1].ano),
            this.somaValorMensalDespesas(this.despesasParlamentar[2].mes, this.despesasParlamentar[2].ano),
            this.somaValorMensalDespesas(this.despesasParlamentar[3].mes, this.despesasParlamentar[3].ano),
            this.somaValorMensalDespesas(this.despesasParlamentar[4].mes, this.despesasParlamentar[4].ano),
            this.somaValorMensalDespesas(this.despesasParlamentar[5].mes, this.despesasParlamentar[5].ano)
          ]
        }
      ]
    }

    this.graficoPizza = {

      labels: [this.nomeDespesa, this.nomeDespesa1, this.nomeDespesa4],
      datasets: [
        {
          data: [this.totalDespesa, this.totalDespesa1, this.totalDespesa4],
          backgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56"
          ],
          hoverBackgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56"
          ]
        }]
    };

  }

}
